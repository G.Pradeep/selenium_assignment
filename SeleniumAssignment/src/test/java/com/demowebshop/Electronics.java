package com.demowebshop;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Electronics {

	public static void main(String[] args) throws AWTException, Throwable {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/login");
		driver.findElement(By.id("Email")).sendKeys("pradeepg123@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("pradeepg");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Electronics')]")).click();
		driver.findElement(By.xpath("(//a[contains(text(),'Cell phones')])[3]")).click();
		driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
		driver.findElement(By.xpath("(//span[text()='Shopping cart'])[1]")).click();
		driver.findElement(By.name("removefromcart")).click();
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();
		driver.findElement(By.xpath("(//input[@value='Continue'])[1]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//input[@value='Continue'])[2]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//input[@value='Continue'])[3]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//input[@value='Continue'])[4]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//input[@value='Continue'])[5]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@value='Confirm']")).click();
		Thread.sleep(3000);
		String text = driver.findElement(By.xpath("//h1[text()='Thank you']")).getText();
		System.out.println(text);
		WebElement t=driver.findElement(By.xpath("//strong[contains(text(),'Your order has been successfully processed!')]"));
		String ak =t.getText();
		System.out.println(ak);
		WebElement text1 = driver.findElement(By.xpath("//li[contains(text(),'Order')]"));
		String ak2 =text1.getText();
		System.out.println(ak2);
		driver.findElement(By.xpath("//a[text()='Log out']")).click();

	}
}