package com.demoorangeHRM;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class OrangeHRMUserMangement {

	public static void main(String[] args) throws Throwable {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(3000);
		driver.findElement(By.name("username")).sendKeys("Admin");
		driver.findElement(By.name("password")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElement(By.xpath("/html/body/div/div[1]/div[1]/aside/nav/div[2]/ul/li[1]/a/span")).click();
		
		driver.findElement(By.xpath("//div[@class='orangehrm-header-container']//child::button[@type='button']")).click();
		
		driver.findElement(By.xpath("//label[contains(text(),'User Role')]//following::i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow'][1]")).click();
		
		Robot r=new Robot();
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@placeholder='Type for hints...']")).sendKeys("Paul  Collings");
		
		driver.findElement(By.xpath("//label[contains(text(),'Status')]//following::i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow'][1]")).click();
		
		Robot r1=new Robot();
		r1.keyPress(KeyEvent.VK_DOWN);
		r1.keyRelease(KeyEvent.VK_DOWN);
		r1.keyPress(KeyEvent.VK_DOWN);
		r1.keyRelease(KeyEvent.VK_DOWN);
		
		
		 driver.findElement(By.xpath("//label[contains(text(),'Username')]//following::input[@class='oxd-input oxd-input--active'][1]")).sendKeys("pradeepg");
		
		driver.findElement(By.xpath("//label[contains(text(),'Password')]//following::input[@class='oxd-input oxd-input--active'][1]")).sendKeys("Pr@d1234");
		
		driver.findElement(By.xpath("//label[contains(text(),'Confirm Password')]//following::input[@class='oxd-input oxd-input--active'][1]")).sendKeys("Pr@d1234");
		
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		driver.findElement(By.xpath("//span[@class='oxd-userdropdown-tab']")).click();
		
		driver.findElement(By.xpath("//a[contains(text(),'Logout')]")).click();
		
	/*	WebDriver driver=new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com");
		driver.manage().window().maximize();
		Thread.sleep(10000);
		
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("Admin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElement(By.xpath("/html/body/div/div[1]/div[1]/aside/nav/div[2]/ul/li[1]/a/span")).click();
		
		driver.findElement(By.xpath("//div[@class='orangehrm-header-container']//child::button[@type='button']")).click();
		
		driver.findElement(By.xpath("//label[contains(text(),'User Role')]//following::i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow'][1]")).click();
		
		Robot r=new Robot();
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		Thread.sleep(10000);
		
		driver.findElement(By.xpath("//input[@placeholder='Type for hints...']")).sendKeys("Paul  Collings");
		
		driver.findElement(By.xpath("//label[contains(text(),'Status')]//following::i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow'][1]")).click();
		
		Robot r1=new Robot();
		r1.keyPress(KeyEvent.VK_DOWN);
		r1.keyRelease(KeyEvent.VK_DOWN);
		r1.keyPress(KeyEvent.VK_DOWN);
		r1.keyRelease(KeyEvent.VK_DOWN);
		
		driver.findElement(By.xpath("//label[contains(text(),'Username')]//following::input[@class='oxd-input oxd-input--active'][1]")).sendKeys("pradeepg");
		
		driver.findElement(By.xpath("//label[contains(text(),'Password')]//following::input[@class='oxd-input oxd-input--active'][1]")).sendKeys("Pr@d1234");
		
		driver.findElement(By.xpath("//label[contains(text(),'Confirm Password')]//following::input[@class='oxd-input oxd-input--active'][1]")).sendKeys("Pr@d1234");
		
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		driver.findElement(By.xpath("//span[@class='oxd-userdropdown-tab']")).click();
		
		driver.findElement(By.xpath("//a[contains(text(),'Logout')]")).click();*/
		

	}

}
